import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestMystere {
    private Noeud[] tableauNoeud;
    private Noeud petitNoeud, moyenNoeud, grandNoeud;
    
    @Before
    public void genererExemples () {
        genererTableauNoeud();
        
        // (1+2)
        petitNoeud = new Noeud('+', tableauNoeud[1], tableauNoeud[2]);
        
        // ((1+2)*3)
        moyenNoeud = new Noeud('*', petitNoeud, tableauNoeud[3]);
        
        // ((4-5)*((1+2)*3))
        grandNoeud = new Noeud('*', new Noeud('-', tableauNoeud[4], tableauNoeud[5]), moyenNoeud);
    }
    
    private void genererTableauNoeud () {
        tableauNoeud = new Noeud[6];
        tableauNoeud[0] = new Noeud('+'); // Chose invalide
        for (byte i = 1; i <= 5; i++)
            tableauNoeud[i] = new Noeud(Character.forDigit(i, 10));
    }
    
    @Test
    public void testNoeudUnique () {
        ExpressionMathematique m = new ExpressionMathematique(tableauNoeud[1]);
        assertEquals("1", m.print());
    }
    
    @Test (expected = RuntimeException.class)
    public void testNoeudNull() {
        ExpressionMathematique m = new ExpressionMathematique(null);
        m.print();
    }    

    @Test (expected = RuntimeException.class)
    public void testNoeudInvalide () {
        ExpressionMathematique m = new ExpressionMathematique(tableauNoeud[0]);
        m.print();
    }
    
    @Test
    public void testPetitNoeud () {
        ExpressionMathematique m = new ExpressionMathematique(petitNoeud);
        assertEquals("(1+2)", m.print());
    }
    
    @Test (expected = RuntimeException.class)
    public void testEnfantNullPetit () {
        petitNoeud.noeudSuivant = null; // (1+null)
        ExpressionMathematique m = new ExpressionMathematique(petitNoeud);
        m.print();
    }

    @Test (expected = RuntimeException.class)
    public void testEnfantInvalidePetit() {
        tableauNoeud[2].charactere = '*'; // (1+*)
        ExpressionMathematique m = new ExpressionMathematique(petitNoeud);
        m.print();
    }
    
    @Test (expected = RuntimeException.class)
    public void testNoeudNumeriquePetit () {
        petitNoeud.charactere = '9'; // (192)
        ExpressionMathematique m = new ExpressionMathematique(petitNoeud);
        m.print();
    }
    
    @Test
    public void testMoyenNoeud() {
        ExpressionMathematique m = new ExpressionMathematique(moyenNoeud);
        assertEquals("((1+2)*3)", m.print());
    }
    
    @Test (expected = RuntimeException.class)
    public void testEnfantNullMoyen () {
        petitNoeud.noeudSuivant = null; // ((1+null)*3)
        ExpressionMathematique m = new ExpressionMathematique(moyenNoeud);
        m.print();
    }
    
    @Test (expected = RuntimeException.class)
    public void testNoeudInvalideMoyen () {
        tableauNoeud[2].charactere = '*'; // ((1+*)*3)
        ExpressionMathematique m = new ExpressionMathematique(moyenNoeud);
        m.print();
    }
    
    @Test (expected = RuntimeException.class)
    public void testNoeudNumeriqueMoyen () {
        petitNoeud.charactere = '9'; // ((192)*3)
        ExpressionMathematique m = new ExpressionMathematique(moyenNoeud);
        m.print();
    }
    
    @Test (timeout = 5)
    public void testGrandNoeud () {
        ExpressionMathematique m = new ExpressionMathematique(grandNoeud);
        assertEquals("((4-5)*((1+2)*3))", m.print());
    }
    
    @Test (timeout = 5)
    public void testPlusieursNoeud () {
        petitNoeud.noeudSuivant = new Noeud('*', new Noeud('6'), new Noeud('7'));
        moyenNoeud.noeudSuivant = new Noeud('-', new Noeud('8'), new Noeud('9'));
        grandNoeud.noeudPrecedent.noeudPrecedent = new Noeud('*', new Noeud('2'), new Noeud('3'));
        ExpressionMathematique m = new ExpressionMathematique(grandNoeud);
        assertEquals("(((2*3)-5)*((1+(6*7))*(8-9)))", m.print());
    }
}
