import java.util.Stack;

public class ExpressionMathematique {
    private Noeud noeudRacine;
    private StringBuilder flag;
    
    public ExpressionMathematique (Noeud noeudRacine) {
        this.noeudRacine = noeudRacine;
    }
    
    public String print() {
        flag = new StringBuilder();
        final Noeud noeudFin = new Noeud(')');
        Stack<Noeud> stackNoeud = new Stack<>();
        
        Noeud noeudCourant = noeudRacine;
        do {
            if (estSansEnfant(noeudCourant)) {
                ajouteChiffre(noeudCourant.charactere);
                
                do {
                    if (stackNoeud.isEmpty())
                        return flag.toString();
                    noeudCourant = stackNoeud.pop();
                    enleveChiffre(noeudCourant.charactere);
                } while (noeudCourant == noeudFin);
                
                noeudCourant = noeudCourant.noeudSuivant;
            }
            else {
                flag.append('(');
                stackNoeud.push(noeudFin);
                stackNoeud.push(noeudCourant);
                noeudCourant = noeudCourant.noeudPrecedent;
            }
        } while (noeudCourant != null);
        
        throw new RuntimeException("Erreur pendant la creation du message");
    }
    
    private boolean estSansEnfant (Noeud noeud) {
        return noeud.noeudPrecedent == null && noeud.noeudSuivant == null;
    }
    
    private void ajouteChiffre (char charactere) {
        if (Character.isDigit(charactere))
            flag.append(charactere);
        else
            throw new RuntimeException("La chose " + charactere + " n'est pas un chiffre.");
    }
    
    private void enleveChiffre (char charactere) {
        if (!Character.isDigit(charactere))
            flag.append(charactere);
        else
            throw new RuntimeException("La chose " + charactere + " est un chiffre.");
    }
}
