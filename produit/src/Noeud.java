public class Noeud {
    public Noeud noeudPrecedent, noeudSuivant;
    public char charactere;
    
    public Noeud (char charactere, Noeud noeudPrecedent, Noeud noeudSuivant) {
        this.noeudPrecedent = noeudPrecedent;
        this.noeudSuivant = noeudSuivant;
        this.charactere = charactere;
    }
    
    public Noeud (char charactere) {
        this.noeudPrecedent = null;
        this.noeudSuivant = null;
        this.charactere = charactere;
    }
}
